import sys
import os
import glob
import re
import itertools
import tempfile as tmp
import subprocess as proc
from collections import namedtuple, defaultdict, Counter, deque
import math
from classifier import cross_validate_attribute_set, train, test, load_feature_transforms_from_config, get_relative_frequencies_from_counter
import yaml
import random

import nltk.tag.stanford as sttag

import numpy as np

import pipeline as pypl

from stats import calculate_pmis

DEFAULT_PDTB_PATH = "../../libs/pdtb-parser/"

@pypl.produces("raw_text", pypl.Token)
@pypl.produces("doc_id", pypl.Document)
@pypl.produces("split", pypl.Document)
@pypl.produces("prompt", pypl.Document)
@pypl.produces("l1", pypl.Document)
class Toefll11CorpusReader(object):
    """
    Reads the requierd text documents from the corpus and creates document objects.
    """
    @staticmethod
    def reader_from_config(config, split):
        """
        Takes information from config file and calls the 
        Toefll11CorpusReader with the extracted arguments.
        
        Args:
            config: the config file
            split: the train, dev, or test split of the corpus
        """
        config_args = {}
        config_args["directory"] = config["directory"]
        prompts = config.get("prompts")
        if prompts:
            config_args["prompts"] = prompts

        proficiencies = config.get("proficiencies")
        if proficiencies:
            config_args["proficiencies"] = proficiencies

        limit = config.get("limit")
        if limit:
            config_args["limit"] = limit

        return Toefll11CorpusReader(split = split, **config_args)

    def __init__(self, directory, file_list = None, split="traindev", prompts = None, proficiencies = None, limit = None):
        if file_list:
            self.filenames = [directory + "/" + fname for fname in file_list]
        else:
            self.filenames = glob.glob(directory+"/*.txt")
        self.split = split

        if prompts:
            self.prompts = set(prompts)
        else:
            self.prompts = None

        if proficiencies:
            self.proficiencies = set(proficiencies)
        else:
            self.proficiencies = None

        self.limit = limit

        if self.limit:
            random.shuffle(self.filenames)

    def __iter__(self):
        doc_cnt = 0
        for idx, fname in enumerate(self.filenames):
            real_name = os.path.basename(fname)

            split, prompt, l1, proficiency, id = real_name[:-4].split("_")

            if self.prompts and prompt not in self.prompts:
                continue

            if self.proficiencies and proficiency not in self.proficiencies:
                continue

            if self.split != split:
                continue

            doc_cnt += 1

            yield Toefll11DocumentPromise(real_name, split, prompt, l1, proficiency, id, fname)

            if self.limit != None and doc_cnt == self.limit:
                break

class Toefll11DocumentPromise:
    """
    Creates a Document odjekt with the following attributes:
    -name, split, promt, proficiency, id, fname, l1
    -sentences, tokens
    
    Args: 
        name
        split: train, test or dev split od the corpus
        promt: promt of corpus
        l1 :  mothertoung of author
        proficiency: english proficiency of the author
        doc_id: ID of document
        fname
    
    Returns: 
        document with annotated senteces and tokens.
    """
    def __init__(self, name, split, prompt, l1, proficiency, doc_id, fname):
        self.name = name
        self.split = split
        self.prompt = prompt
        self.l1 = l1
        self.proficiency = proficiency
        self.doc_id = doc_id
        self.fname = fname

    def __call__(self):
        doc = pypl.Document()
        doc.name = self.name
        doc.attrs.split = self.split
        doc.attrs.prompt = self.prompt
        doc.attrs.l1 = self.l1
        doc.attrs.proficiency = self.proficiency

        doc.attrs.doc_id = self.doc_id
        curr_tok_idx = 0

        for sent in self.read_file(self.fname):
            sentence = pypl.Sentence()
            for tok in sent.split():
                token = pypl.Token()
                token.idx = curr_tok_idx
                doc.add_token(token)
                sentence.add_token(token)
                token.attrs.raw_text = tok
                curr_tok_idx += 1
            if len(sentence.tokens) > 0:
                    doc.add_span(sentence)

        return doc

    def read_file(self, filename):
        with open(filename) as f:
            return f.readlines()

@pypl.requires("raw_text", pypl.Token)
@pypl.produces("pos_tag", pypl.Token)
class POSTagger:
    """
    Tags a given dokument with Part of Speech Tags with the Stanford POS Tagger.
    """
    def __init__(self):
        self.tagger = sttag.POSTagger("../english-bidirectional-distsim.tagger", path_to_jar = "../stanford-postagger.jar", java_options='-Xmx4G', encoding="utf8")

    def __call__(self, doc):
        tagged_tokens = self.tagger.tag(map(lambda t: t.attrs.raw_text, doc.tokens))

        for toc_doc, toc_tagged in zip(doc.tokens, tagged_tokens):
            _, pos = toc_tagged
            toc_doc.attrs.pos_tag = pos


def write_doc_to_temporary_textfile(doc):
    f_tmp = tmp.NamedTemporaryFile(delete=False)
    filename = f_tmp.name
    for sent in doc.get_all_annotation_items_at_level(pypl.Sentence):
        f_tmp.write(" ".join(map(lambda t: t.attrs.raw_text, sent.tokens)))
        f_tmp.write("\n")
    return filename

class PDTBRelation(pypl.TokenSpan):
    def __init__(self):
        super(PDTBRelation, self).__init__()


def lines_from_process(p):
    with p.stdout:
        #See http://stackoverflow.com/questions/2715847/python-read-streaming-input-from-subprocess-communicate
        for l in iter(p.stdout.readline, ""):
            yield l
        p.wait()

def unescape_string(s):
    new_chars = []
    idx = 0
    while idx < len(s):
        c = s[idx]
        if not (idx + 1 < len(s) and c == "\\" and s[idx + 1] == "/"):
            new_chars.append(c)
        idx += 1
    return "".join(new_chars)

def get_ngram_counts(iter_, n, placeholder = None):
    ngram_counter = Counter()

    curr_ngram = deque()
    for _ in range(0, n - 1):
        curr_ngram.append(placeholder)

    for obj in iter_:
        curr_ngram.append(obj)
        ngram_counter[tuple(curr_ngram)] += 1
        curr_ngram.popleft()

    for i in range(0, n - 1):
        curr_ngram.append(placeholder)
        ngram_counter[tuple(curr_ngram)] += 1
        curr_ngram.popleft()

    for key in ngram_counter:
        if len(key) == 1 and callable(key[0]):
            print "AHA!"
    return ngram_counter

class FeatureNGramFrequencyCounter:
    @property
    def pypline__produced_attributes(self):
        return [pypl.Attr(self.ngram_frequency_attr_key(), pypl.Document)]

    @property
    def pypline__required_attributes(self):
        return [pypl.Attr(self.attr_key, self.level)]

    def ngram_frequency_attr_key(self):
        if self.attr_prefix:
            prefix = self.attr_prefix
        else:
            prefix = self.attr_key

        if self.use_absolute_counts:
            prefix = "absolute_" + prefix

        return "{}_{}gram_frequencies".format(prefix, self.n)

    def __init__(self, level, attr_key, n, placeholder = None, filter_func = None, attr_prefix = None, use_absolute_counts = False):
        self.level = level
        self.attr_key = attr_key
        self.n = n
        self.placeholder = placeholder
        self.use_absolute_counts = use_absolute_counts
        if filter_func == None:
            filter_func = lambda x: True
        elif not attr_prefix:
            raise RuntimeError("Cannot initialize filtered FeatureNGramFrequencyCounter without explicit attribute attr_prefix")
        self.filter_func = filter_func
        self.attr_prefix = attr_prefix

    def __call__(self, doc):
        annotation_objects = doc.get_all_annotation_items_at_level(self.level)

        ngram_counter = get_ngram_counts(map(lambda o: o.attrs[self.attr_key], filter(self.filter_func, annotation_objects)), self.n, placeholder = self.placeholder)
        
        if self.use_absolute_counts:
            doc.attrs[self.ngram_frequency_attr_key()] = ngram_counter
        else:
            relative_frequencies = get_relative_frequencies_from_counter(ngram_counter)
            doc.attrs[self.ngram_frequency_attr_key()] = relative_frequencies

@pypl.requires("raw_text", pypl.Token)
@pypl.requires_span_level(pypl.Sentence)
@pypl.creates_span_level(PDTBRelation)
@pypl.produces("idx", PDTBRelation) #Integer
@pypl.produces("arg1", PDTBRelation) #List of tokens
@pypl.produces("arg2", PDTBRelation) #List of tokens
@pypl.produces("rel_type", PDTBRelation) #"implicit" or "explicit"
@pypl.produces("sense", PDTBRelation) #sense (as a string)
@pypl.produces("connective", PDTBRelation) #connective (Token(s)) or None (for implicit relations)
class PDTBParser:
    """
    Parses the document with a PDTB parser. Saves arg1, arg2 , 
    relation type (implicit or explicit), sense  
    and connective (Token(s)) or None (for implicit relations) 
    in the documents attributes.
    """
    PDTB_ANNOTATION_OPEN_REGEX = re.compile(r"{(?P<type>Exp|NonExp|Attr)\_(?P<id>\d+)(\_(?P<elem>Arg1|Arg2|conn))?(\_(?P<sense>\w+))?")
    PDTB_ANNOTATION_CLOSE_REGEX = re.compile(r"(?P<type>Exp|NonExp|Attr)\_(?P<id>\d+)(\_(?P<elem>Arg1|Arg2|conn))?}")

    def __init__(self, pdtb_base_path, results_path = "pdtb-parser-output/"):
        self._pdtb_base_path = pdtb_base_path
        self._pdtb_parse_path = pdtb_base_path + "/src/parse.rb"
        self.results_path = results_path
        if not os.path.exists(self.results_path):
            os.mkdir(self.results_path)

    def path_for_outfile(self, doc_name):
        return self.results_path + "/" + doc_name + ".pdtb_parse"

    def __call__(self, doc):
        try:
            with open(self.path_for_outfile(doc.name)) as f_in:
                lines = f_in.readlines()
        except IOError:
            filename = write_doc_to_temporary_textfile(doc)
            parser = proc.Popen([self._pdtb_parse_path, filename], stderr = sys.stderr, stdout = proc.PIPE)
            lines = list(lines_from_process(parser))
            with open(self.path_for_outfile(doc.name), "w") as f_out:
                for line in lines:
                    f_out.write(line)
            os.remove(filename)
        self.read_parsed_pdtb_stream(doc, iter(lines))

    def read_parsed_pdtb_stream(self, doc, stream):
        """
        Reads the PDTB output and extracts attributes.
        
        Args:
            doc: a document
            stream
        """
        token_idx = 0
        tokens = doc.all_tokens
        curr_paragraph_tokens = []

        stop = False
        while not stop:
            try:
                line = next(stream)
                line = line.strip()
                while len(line) == 0:
                    line = next(stream)
                    line = line.strip()
            except StopIteration:
                stop = True
                line = None

            if line == None:
                relations, pdtb_tokens = self.parse_pdtb_paragraph(curr_paragraph_tokens, token_idx)

                pdtb_doc_match_map = self.match_pdtb_tokens_to_doc_indices(doc, pdtb_tokens)
                token_idx += len(pdtb_tokens)

                for idx, relation in relations.items():
                    rel = PDTBRelation()

                    rel.attrs.idx = idx
                    rel.attrs.sense = relation.sense
                    if relation.type_ == "Exp":
                        rel.attrs.rel_type = "explicit"
                    else:
                        rel.attrs.rel_type = "implicit"


                    rel.attrs.connective = pdtb_doc_match_map[relation.connective_start:relation.connective_end] #self.convert_pdtb_range_to_doc_tokens(doc, relation.connective_start, relation.connective_end, token_doc_indices)
                    rel.attrs.arg1 = pdtb_doc_match_map[relation.arg1_start:relation.arg1_end] #self.convert_pdtb_range_to_doc_tokens(doc, relation.arg1_start, relation.arg1_end, token_doc_indices)
                    rel.attrs.arg2 = pdtb_doc_match_map[relation.arg2_start:relation.arg2_end] #self.convert_pdtb_range_to_doc_tokens(doc, relation.arg2_start, relation.arg2_end, token_doc_indices)

                    rel.tokens = rel.attrs.connective + rel.attrs.arg1 + rel.attrs.arg2

                    doc.add_span(rel)

                curr_paragraph_tokens = []
                continue
            else:
                curr_paragraph_tokens.extend(line.split(" "))

    def convert_pdtb_range_to_doc_tokens(self, doc, range_start, range_end, token_doc_indices):
        doc_toks = []
        previous = None
        for idx in token_doc_indices[range_start:range_end]:
            if idx != previous:
                doc_toks.append(doc.tokens[idx])
            previous = idx
        return doc_toks

    def match_pdtb_tokens_to_doc_indices(self, doc, pdtb_tokens):
        token_map = []
        equivalent_tokens = {
                ("(", "-LRB-"),
                (")", "-RRB-"),
                ("/", r"\/"),
        }

        if len(doc.tokens) == len(pdtb_tokens):
            token_map = []
            for doc_tok, pdtb_tok in zip(doc.tokens, pdtb_tokens):
                token_map.append(doc_tok)
            return token_map

        def eql_cost(row_idx, column_idx):
            parser_text = pdtb_tokens[row_idx - 1]
            orig_text = doc.tokens[column_idx - 1].attrs.raw_text
            if parser_text == orig_text \
            or (orig_text, parser_text) in equivalent_tokens:
                return 0
            elif row_idx > 1 and pdtb_tokens[row_idx - 1] + pdtb_tokens[row_idx - 2] == orig_text:
                return 0
            else:
                return 2

        operations = [
                ("eq", 1, 1, eql_cost),
                ("skip_parser", 0, 1, lambda x, y: 1),
                ("skip_orig", 1,  0, lambda x, y: 1),
                ]

        cost_matrix = np.array([[0] * (len(doc.tokens) + 1) for _ in range(len(pdtb_tokens) + 1)])
        back_pointers = [[(0, 0)] * (len(doc.tokens) + 1) for _ in range(len(pdtb_tokens) + 1)]

        for row_idx in range(len(pdtb_tokens) + 1):
            cost_matrix[row_idx,0] = row_idx
        for column_idx in range(len(doc.tokens) + 1):
            cost_matrix[0,column_idx] = column_idx

        for row_idx in range(1, len(pdtb_tokens) + 1):
            for column_idx in range(1, len(doc.tokens) + 1):
                values = []
                for key, x, y, cost_func in operations:
                    values.append((key, row_idx - x, column_idx - y, cost_matrix[row_idx - x, column_idx - y] + cost_func(row_idx, column_idx)))
                key, from_row_idx, from_col_idx, cost = min(*values, key=lambda x: x[3])
                cost_matrix[row_idx, column_idx] = cost
                back_pointers[row_idx][column_idx] = (key, from_row_idx, from_col_idx)

        curr_row_idx = len(pdtb_tokens)
        curr_column_idx = len(doc.tokens)
        path = []
        while curr_row_idx != 0 and curr_column_idx != 0:
            key, from_row_idx, from_col_idx = back_pointers[curr_row_idx][curr_column_idx]
            curr_row_idx = from_row_idx
            curr_column_idx = from_col_idx

            path.append(key)
        path.reverse()

        token_map = []
        doc_cursor = 0

        for step in path:
            if step == "eq":
                token_map.append(doc.tokens[doc_cursor])
                doc_cursor += 1
            elif step == "skip_parser":
                token_map[-1] = doc.tokens[doc_cursor]
            elif step == "skip_orig":
                token_map.append(doc.tokens[doc_cursor])
        return token_map

    def match_pdtb_tokens_to_doc_indices__old(self, doc, pdtb_tokens):
        """
        Takes the tokens produces by the pdtb parser and matches them 
        back to the original ones from the document.
        
        Args:
            doc: the document
            pdtb_tokens: list, tokens produced by the PDTB parser
        
        Returns:
            token_indices: the indicies of of the tokens in the document 
            which match the PDTB parser output.
        
        Raises:
            Runtime Error if there are PDTB tokens remaining but no more in the document.
        """
        token_indices = []
        doc_tok_idx = 0
        pdtb_tok_idx = 0
        while pdtb_tok_idx < len(pdtb_tokens):
            if doc_tok_idx >= len(doc.tokens):
                raise RuntimeError("Remaining tokens {} can't be matched. No more tokens in document".format(
                    pdtb_tokens[pdtb_tok_idx:]
                    ))
            pdtb_tok = unescape_string(pdtb_tokens[pdtb_tok_idx])
            doc_tok = doc.tokens[doc_tok_idx]
            if doc_tok.attrs.raw_text != pdtb_tok and doc_tok_idx != 0 and not (doc_tok.attrs.raw_text == ".." and pdtb_tok == "."):
                #TODO: Generalize
                if (pdtb_tok_idx + 1 < len(pdtb_tokens) \
                        and doc_tok.attrs.raw_text == pdtb_tok + pdtb_tokens[pdtb_tok_idx + 1]):
                    pdtb_tok_idx += 2
                    token_indices.append(doc_tok_idx)
                    token_indices.append(doc_tok_idx)
                    doc_tok_idx += 1
                elif (pdtb_tok_idx + 2 < len(pdtb_tokens) \
                        and doc_tok.attrs.raw_text == pdtb_tok + pdtb_tokens[pdtb_tok_idx + 1] + pdtb_tokens[pdtb_tok_idx + 2]):
                    pdtb_tok_idx += 3
                    token_indices.append(doc_tok_idx)
                    token_indices.append(doc_tok_idx)
                    token_indices.append(doc_tok_idx)
                    doc_tok_idx += 1
                else:
                    print "{}: Could not match token '{}' against {} in document at index {} from PDTB output.".format(
                        doc.name,
                        pdtb_tok,
                        doc_tok,
                        doc_tok_idx,
                        )
                    token_indices.append(doc_tok_idx)
                    doc_tok_idx += 1
                    pdtb_tok_idx += 1
                    #raise RuntimeError("Could not match token '{}' against {} in document at index {} from PDTB output.".format(
                    #    pdtb_tok,
                    #    doc_tok,
                    #    doc_tok_idx))
            elif doc_tok.attrs.raw_text != pdtb_tok and pdtb_tok == "." and pdtb_tok_idx > 0 and pdtb_tokens[pdtb_tok_idx - 1].endswith("."):
                token_indices.append(doc_tok_idx)
                pdtb_tok_idx += 1
            else:
                token_indices.append(doc_tok_idx)
                doc_tok_idx += 1
                pdtb_tok_idx += 1

        if doc_tok_idx < len(doc.tokens):
            raise RuntimeError("Remaining tokens {} in document can't be matched. No more tokens in PDTB output.".\
                    format(doc.tokens[doc_tok_idx:]))

        return token_indices

    def parse_pdtb_paragraph(self, tokens, token_idx):
        """
        Args:
            tokens: a list of all tokens of a paragraph
            token_idx: the index of all tokens in the list tokens.
        
        Returns:
            relations: dictionary, contains the pdtb relations from the given paragraph
            pdtb_tokens: list, tokens produced by the PDTB parser
            
        Raises: 
            RuntimeError when a unknoen PDTB element was found.
        """
        next_token_idx = token_idx
        relations = {}
        pdtb_tokens = []
        for token in tokens:
            open_match = re.match(PDTBParser.PDTB_ANNOTATION_OPEN_REGEX, token)
            if open_match:
                if open_match.group("type") == "Attr":
                    continue
                rel = relations.get(open_match.group("type") + "_" + open_match.group("id"))
                if not rel:
                    rel = PDTBTempRelation()
                    rel.type_ = open_match.group("type")
                    relations[open_match.group("type") + "_" + open_match.group("id")] = rel

                if open_match.group("sense") is not None:
                    rel.sense = open_match.group("sense")

                if open_match.group("elem") == "Arg1":
                    rel.arg1_start = next_token_idx
                elif open_match.group("elem") == "Arg2":
                    rel.arg2_start = next_token_idx
                elif open_match.group("elem") == "conn":
                    rel.connective_start = next_token_idx
                else:
                    raise RuntimeError("Unknown PDTB element '{}'".format(open_match.group("elem")))

                continue

            close_match = re.match(PDTBParser.PDTB_ANNOTATION_CLOSE_REGEX, token)
            if close_match:
                if close_match.group("type") == "Attr":
                    continue
                rel = relations.get(close_match.group("type") + "_" + close_match.group("id"))
                if not rel:
                    raise RuntimeError("Mismatched closing bracket '{}'".format(close_match.group("elem")))

                if close_match.group("elem") == "Arg1":
                    rel.arg1_end = next_token_idx
                elif close_match.group("elem") == "Arg2":
                    rel.arg2_end = next_token_idx
                elif close_match.group("elem") == "conn":
                    rel.connective_end = next_token_idx
                else:
                    raise RuntimeError("Unknown PDTB element '{}'".format(close_match.group("elem")))

                continue

            next_token_idx += 1
            pdtb_tokens.append(token)

        return relations, pdtb_tokens

class PDTBTempRelation:
    def __init__(self):
        self.connective_start = None
        self.connective_end = None

@pypl.requires("per_sense_arg1_first_counts", pypl.Document)
@pypl.requires("per_sense_arg2_first_counts", pypl.Document)
@pypl.produces("relative_arg_1_precedence_ratio", pypl.Document)
class RelativeArg1FirstRatioCalculator:
    """
    Counts the how often the first argument occours before 
    the second argument and vise versa in a given dokument.
    """
    def __call__(self, doc):
        total_arg1_first_cnt = sum(doc.attrs.per_sense_arg1_first_counts.values())
        total_arg2_first_cnt = sum(doc.attrs.per_sense_arg2_first_counts.values())

        if total_arg1_first_cnt + total_arg2_first_cnt == 0:
            doc.attrs.relative_arg_1_precedence_ratio = 0.0
        else:
            doc.attrs.relative_arg_1_precedence_ratio = total_arg1_first_cnt / float(total_arg1_first_cnt + total_arg2_first_cnt)

@pypl.requires("per_sense_arg1_first_counts", pypl.Document)
@pypl.requires("per_sense_arg2_first_counts", pypl.Document)
@pypl.produces("per_sense_relative_arg_1_precedence_ratios", pypl.Document)
class PerSenseRelativeArg1FirstRatioCalculator:
    """
    Computes the relative frequency of arg1
    """
    def __call__(self, doc):
        results = {}

        all_keys = set(doc.attrs.per_sense_arg1_first_counts).union(set(doc.attrs.per_sense_arg2_first_counts))
        for key in doc.attrs.per_sense_arg1_first_counts:
            arg1_frist_cnt = doc.attrs.per_sense_arg1_first_counts[key]
            arg2_frist_cnt = doc.attrs.per_sense_arg2_first_counts[key]
            results[key] = arg1_frist_cnt / float(arg1_frist_cnt + arg2_frist_cnt)

        doc.attrs.per_sense_relative_arg_1_precedence_ratios = results

@pypl.requires("raw_text", pypl.Token)
class CharacterNGramCounter:
    """
    Computes character n-grams from a document.
    """
    def __init__(self, n, placeholder = None, use_absolute_counts = False):
        self.n = n
        self.use_absolute_counts = use_absolute_counts

    def __call__(self, doc):
        ngram_counter = Counter()
        tokens = doc.get_all_annotation_items_at_level(pypl.Token)

        for token in tokens:
            ngrams = self.extract_char_ngrams(token)
            for ngram in ngrams:
                ngram_counter[ngram] += 1

        if self.use_absolute_counts:
            doc.attrs[self.ngram_frequency_attr_key()] = ngram_counter
        else:
            doc.attrs[self.ngram_frequency_attr_key()] = get_relative_frequencies_from_counter(ngram_counter)

    def extract_char_ngrams(self, token):
        return get_ngram_counts(token.attrs.raw_text, self.n)

    @property
    def pypline__produced_attributes(self):
        return [pypl.Attr(self.ngram_frequency_attr_key(), pypl.Document)]

    def ngram_frequency_attr_key(self):
        prefix = ""
        if self.use_absolute_counts:
            prefix = "absolute_"
        return "{}character_{}gram_frequencies".format(prefix, self.n)

@pypl.requires("raw_text", pypl.Token)
@pypl.produces("lower_cased_text", pypl.Token)
class CapitalizationTool:
    """
    Lowercases the text in a given document. 
    """
    def __call__(self, doc):
        tokens = doc.get_all_annotation_items_at_level(pypl.Token)

        for token in tokens:
            token.attrs.lower_cased_text = token.attrs.raw_text.lower()

@pypl.requires("arg1", PDTBRelation)
@pypl.requires("arg2", PDTBRelation)
@pypl.requires("rel_type", PDTBRelation)
@pypl.requires("sense", PDTBRelation)
@pypl.produces("per_sense_arg1_first_counts", pypl.Document)
@pypl.produces("per_sense_arg2_first_counts", pypl.Document)
class PerSenseLeftRightArgumentPositionCounter:
    """
    Constructs 3-grams to capture the order of apperance of Arg1, Arg2, and sense.
    """
    def __init__(self, level):
        self.level = level
    def __call__(self, doc):
        arg1_first_counter = Counter()
        arg2_first_counter = Counter()
        for relation in doc.get_all_annotation_items_at_level(PDTBRelation):
            if relation.attrs.rel_type != "explicit":
                continue

            if relation.attrs.arg1[0].idx < relation.attrs.arg2[0].idx:
                arg1_first_counter[self.get_sense_from_relation(relation)] += 1
            else:
                arg2_first_counter[self.get_sense_from_relation(relation)] += 1

        doc.attrs.per_sense_arg1_first_counts = arg1_first_counter
        doc.attrs.per_sense_arg2_first_counts = arg2_first_counter

    def get_sense_from_relation(self, relation):
        if self.level == 2:
            return relation.attrs.sense

class StringToIntConverter:
    def __init__(self):
        self.previous_val_ids = {}
        self.max_val = 0

    def __call__(self, val):
        val_id = self.previous_val_ids.get(val)
        if not val_id:
            val_id = self.previous_val_ids[val] = self.max_val
        return val_id

@pypl.requires("lower_cased_text", pypl.Token)
@pypl.requires("connective", PDTBRelation)
@pypl.requires("rel_type", PDTBRelation)
@pypl.requires("sense", PDTBRelation)
@pypl.produces("absolute_sense_tag_realization_frequencies", pypl.Document)
@pypl.produces("sense_tag_realization_likelihood", pypl.Document)
class SenseTagRealisationCounter:
    """
    Counts the relative frequency on how often a certain sense was 
    realized with a certain token.  
    """
    def __init__(self):
        pass
        #self.level = level
    def __call__(self, doc):
        sense_tok_dict={}
        sense_dict={}
        for relation in doc.get_all_annotation_items_at_level(PDTBRelation):
            if relation.attrs.rel_type == "explicit":
                self.add_to_dict(sense_dict,relation.attrs.sense)
                self.add_to_dict(sense_tok_dict, (relation.attrs.sense, " ".join(map(lambda t: t.attrs.lower_cased_text, relation.attrs.connective))))

        sense_realization_likelihoods = {}
        for sense_tok, sense_freq in sense_tok_dict.items():
            sense_realization_likelihoods[sense_tok] = float(sense_dict[sense_tok[0]])/sense_freq #freg(S,T)/feq(S)

        doc.attrs.sense_tag_realization_likelihood = sense_tok_dict
        doc.attrs.absolute_sense_tag_realization_frequencies = sense_tok_dict

    def add_to_dict(self, dictionary, key):
        if key in dictionary:
            dictionary[key] += 1
        else:
            dictionary[key] = 1

def create_pipeline(reader):
    """
    Creates the pipeline.
    
    Returns:
        pipeline
    """
    token_1gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 1)
    token_2gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 2)
    token_3gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 3)
    token_4gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 4)
    token_5gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 5)
    token_6gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 6)

    absolute_token_1gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 1, use_absolute_counts = True)
    absolute_token_2gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 2, use_absolute_counts = True)
    absolute_token_3gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "lower_cased_text", 3, use_absolute_counts = True)

    absolute_pos_1gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 1, use_absolute_counts = True)
    absolute_pos_2gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 2, use_absolute_counts = True)
    absolute_pos_3gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 3, use_absolute_counts = True)

    absolute_character_2gram_counter = CharacterNGramCounter(2, use_absolute_counts = True)
    absolute_character_3gram_counter = CharacterNGramCounter(3, use_absolute_counts = True)

    sense_2gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 2)
    sense_3gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 3)

    sense_2gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 2)
    sense_3gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 3)

    absolute_sense_1gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 1, use_absolute_counts = True)

    absolute_explicit_sense_2gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 2, filter_func = lambda rel: rel.attrs.rel_type == "explicit", attr_prefix = "explicit_sense", use_absolute_counts = True)
    absolute_explicit_sense_3gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 3, filter_func = lambda rel: rel.attrs.rel_type == "explicit", attr_prefix = "explicit_sense", use_absolute_counts = True)

    absolute_implicit_sense_2gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 2, filter_func = lambda rel: rel.attrs.rel_type == "implicit", attr_prefix = "implicit_sense", use_absolute_counts = True)
    absolute_implicit_sense_3gram_counter = FeatureNGramFrequencyCounter(PDTBRelation, "sense", 3, filter_func = lambda rel: rel.attrs.rel_type == "implicit", attr_prefix = "implicit_sense", use_absolute_counts = True)

    character_2gram_counter = CharacterNGramCounter(2)
    character_3gram_counter = CharacterNGramCounter(3)
    character_4gram_counter = CharacterNGramCounter(4)
    character_5gram_counter = CharacterNGramCounter(5)
    character_6gram_counter = CharacterNGramCounter(6)

    pos_1gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 1)
    pos_2gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 2)
    pos_3gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 3)
    pos_4gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 4)
    pos_5gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 5)
    pos_6gram_counter = FeatureNGramFrequencyCounter(pypl.Token, "pos_tag", 6)

    absolute_sense_tag_realization_counter = SenseTagRealisationCounter()

    pdtb_path = os.environ.get('PDTB_PARSER_PATH')
    if not pdtb_path:
        print "WARNING:", "Environment variable PDTB_PARSER_PATH has not been set, using default location. this is probably not appropriate for your installation."
        pdtb_path = DEFAULT_PDTB_PATH

    pipeline = pypl.Pipeline(reader, [
        PDTBParser(pdtb_path),
        POSTagger(),
        PerSenseLeftRightArgumentPositionCounter(2),
        RelativeArg1FirstRatioCalculator(),
        PerSenseRelativeArg1FirstRatioCalculator(),

        absolute_token_1gram_counter,
        absolute_token_2gram_counter,
        absolute_token_3gram_counter,

        absolute_pos_1gram_counter,
        absolute_pos_2gram_counter,
        absolute_pos_3gram_counter,

        absolute_character_2gram_counter,
        absolute_character_3gram_counter,

        absolute_sense_1gram_counter,

        absolute_explicit_sense_2gram_counter,
        absolute_explicit_sense_3gram_counter,

        absolute_implicit_sense_2gram_counter,
        absolute_implicit_sense_3gram_counter,

        absolute_sense_tag_realization_counter,

        CapitalizationTool()
        ], ignore_exceptions=False, serializer = pypl.Serializer("documents"))
    return pipeline

def run_configuration_in_file(configfile):
    """
    Takes the configfile and starts the configuration.
    
    Args: 
        configfile: a yaml confguration file. It contains the path to the corpus,
                    the features, a model, the class attributa and the test method.
    """
    with open(configfile, "r") as stream:
        config = yaml.load(stream)

    run_configuration(config)

def run_configuration(config):
    """
    Runs a yaml configration, creates the pipeline and runs it.
    
    Args: 
        config: a yaml configuration
    """
    train_reader = Toefll11CorpusReader.reader_from_config(config["reader"], "traindev")

    pipeline = create_pipeline(train_reader)
    train_docs = pipeline.run()

    if config["test_method"] == "crossvalidate":
        cross_validate_attribute_set(train_docs, config["features"], config["class_attr"], feature_transform_config = config["transforms"])
    elif config["test_method"] == "traintest":
        train(train_docs,
            config["features"], config["class_attr"],
            config["model"], feature_transform_config = config["transforms"])

        test_reader = Toefll11CorpusReader.reader_from_config(config["reader"], "test")
        pipeline = create_pipeline(test_reader)
        test_docs = pipeline.run()
        test(test_docs,
            config["features"], config["class_attr"],
            config["model"])

if __name__ == "__main__":
    run_configuration_in_file(sys.argv[1])
