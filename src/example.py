from collections import Counter
from pipeline import produces, requires, creates_span_level, Pipeline, Token, Document, Attr, Sentence

@produces("pos_tag", Token)
@requires("raw_text", Token)
class POSTagger(object):
    def __call__(self, doc):
        for token in doc.all_tokens:
            if token.attrs.raw_text[0].isupper():
                token.attrs.pos_tag = "BL"
            else:
                token.attrs.pos_tag = "SL"

@produces("parent", Token)
@produces("label", Token)
@requires("pos_tag", Token)
@requires("raw_text", Token)
class Parser(object):
    def __call__(self, doc):
        print(doc)
        print(doc.all_tokens)

@produces("raw_text", Token)
@produces("doc_id", Document)
@creates_span_level(Sentence)
class MockCorpusReader(object):
    TOP_OUTPUT_LEVEL = Document

    def __iter__(self):
        texts = [
            [
            "This is a sentence .",
            "This is another one ."
            ],
            [
                "The sentences here serve to test the system ."
            ],
            [
                "This sentence has a connective , because we need to test that too .",
                "As this sentence has it the other way round , we can now test that , too ."
            ],
            ]
        for idx, text in enumerate(texts):
            doc = Document()
            doc.attrs.doc_id = idx
            curr_tok_idx = 0

            for sent in text:
                sentence = Sentence()
                doc.add_span(sentence)
                for tok in sent.split():
                    token = Token()
                    token.idx = curr_tok_idx
                    doc.add_token(token)
                    sentence.add_token(token)
                    token.attrs.raw_text = tok
                    curr_tok_idx += 1
            yield doc

class NominalAttrStatisticsCollector(object):
    def __init__(self, attr_key, level):
        self.attr_key = attr_key
        self.level = level
        self.observed_values = Counter()

    @property
    def pypline__required_attributes(self):
        return [Attr(self.attr_key, self.level)]

    @property
    def pypline__produced_attributes(self):
        return []

    def __call__(self, doc):
        for item in doc.get_all_annotation_items_at_level(self.level):
            self.observed_values[item.attrs[self.attr_key]] += 1

    def __str__(self):
        return "\n".join(map(lambda i: "{}: {}".format(i[0], i[1]), self.observed_values.items()))

@produces("length", Sentence)
class SentenceLengthCounter(object):
    def __call__(self, doc):
        for sent in doc.get_all_annotation_items_at_level(Sentence):
            sent.attrs.length = len(sent.tokens)

if __name__ == "__main__":
    token_stats = NominalAttrStatisticsCollector("pos_tag", Token)
    sent_stats = NominalAttrStatisticsCollector("length", Sentence)
    pipeline = Pipeline(MockCorpusReader(), [POSTagger(), Parser(), sent_stats, SentenceLengthCounter(), token_stats])
    list(pipeline.run())
    print "POS:"
    print token_stats
    print "Lengths:"
    print sent_stats
