'''
Created on 10 Dec 2015

@author: Emmerich
'''
import numpy as np
from pipeline import Document

from collections import Counter

from sklearn.naive_bayes import MultinomialNB
from sklearn import svm
from sklearn import tree
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import LabelEncoder
from sklearn import cross_validation
from sklearn.feature_selection import SelectKBest, chi2
from sklearn.metrics import confusion_matrix, classification_report
from sklearn.externals import joblib

import shutil

import numpy
import os
import pprint
import math
import re

def load_feature_transforms_from_config(config, available_feature_names):
    """
    Loads transforms from a config dict.

    Args:
        config (dict): The config dict to load
        available_feature_names (array): Feature names available to apply transforms to

    Returns:
        A dict with feature keys as keys and arrays of transform callables as values
    """
    transforms_dict = {}
    for feature_name, feat_transform_config in config.items():
        if feature_name in available_feature_names:
            transforms_dict[feature_name] = load_feature_transform_objects_from_config(feat_transform_config)
        else:
            has_one_match = False
            for avail_feat_name in available_feature_names:
                if re.match(feature_name, avail_feat_name):
                    has_one_match = True
                    transforms_dict[avail_feat_name] = load_feature_transform_objects_from_config(feat_transform_config)
            if not has_one_match:
                print "WARNING:", "No feature found matching regex", feature_name

    return transforms_dict

def load_feature_transform_objects_from_config(transforms_config):
    """
    Loads and configures a transform from a config dict.

    Args:
        transforms_config (dict): The config dict to load

    Returns:
        A fully configured transform callable
    """
    TRANSFORM_CLASSES = {"global_frequency_cutoff": FeatureFrequencyFilterTransform,
            "log_entropy_weighting": LogEntropyWeightTransform,
            "relative_weighting": RelativeWeightTransform}

    transforms = []

    for trans_config in transforms_config:
        if isinstance(trans_config, dict):
            transform_key = trans_config.keys()[0]
            parameters = trans_config.values()[0]
        else:
            transform_key = trans_config
            parameters = {}
        class_ = TRANSFORM_CLASSES[transform_key]

        transforms.append(class_(**parameters))

    return transforms

def get_relative_frequencies_from_counter(counter):
    """
    Converts absolute values of a counter to relative values

    Args:
        counter (dict): the counter to transform

    Returns:
        A dict of floats, where sum(dict.values()) == 1.0 and dict.keys() == counter.keys()
    """
    relative_frequencies = {}
    total_count = sum(counter.values())

    for key, cnt in counter.items():
        relative_frequencies[key] = float(cnt) / total_count
    return relative_frequencies

class RelativeWeightTransform:
    """
    Relative Feature Weighter
    """
    def train(self, feat_values):
        pass

    def apply(self, feat_values):
        return get_relative_frequencies_from_counter(feat_values)

class FeatureFrequencyFilterTransform:
    """
    Feature frequency filter
    """
    def __init__(self, threshold):
        self.feat_freq_counter = Counter()
        self.threshold = threshold

    def train(self, feat_values):
        for key in feat_values:
            self.feat_freq_counter[key] += 1

    def apply(self, feat_values):
        return dict(
                filter(lambda kv: self.feat_freq_counter[kv[0]] >= self.threshold, feat_values.items()))

class LogEntropyWeightTransform:
    """
    Log entropy Weighter
    """
    def __init__(self):
        self.global_frequencies = Counter()
        self.doc_count = 0
        self.term_weights = None

        self.doc_term_freqs = []

    def train(self, feat_values):
        self.doc_term_freqs.append(feat_values)

        self.global_frequencies.update(feat_values)

        self.doc_count += 1

        if self.term_weights:
            #Reset term weights with new training examples
            self.term_weights = None

    def _calc_term_weights(self):
        term_weights = {}

        for term in self.global_frequencies.keys():
            term_weights[term] = self._calc_term_weight(term)

        return term_weights

    def _calc_term_weight(self, term):
        sum_ = 0.0
        term_global_freq = self.global_frequencies[term]
        for term_freq_dict in self.doc_term_freqs:
            term_freq = term_freq_dict.get(term, 0)
            p_ij = float(term_freq) / term_global_freq
            if p_ij > 0:
                sum_ += (p_ij * math.log(p_ij) / math.log(self.doc_count))

        return 1 + sum_


    def apply(self, feat_values):
        if not self.term_weights:
            self.term_weights = self._calc_term_weights()

        for key, val in feat_values.items():
            feat_values[key] = self.term_weights[key]

        return feat_values

def retrieve_feature_dict_values(feat_dict, attr_key):
    """
    Gets value from a feature dict. If the value is a dict, the dict is flattened into an array of tuples of the form (attr_key, dict_key).
    Otherwise returns a singelton dict {attr_key: feat_dict[attr_key]}

    Args:
        * feat_dict (dict): The dict to retrieve from
        * attr_key (str): The key to retrieve from feat_dict
    """
    attr_val = feat_dict[attr_key]
    if isinstance(attr_val, dict):
        result = {}
        for key, val in attr_val.items():
            result[(attr_key, key)] = val
        return result
    else:
        return {attr_key: attr_val}


def convert_document_to_feature_dict(doc, attribute_names, class_attr_name):
    """
    Converts a document into a dict by extracting attribute_names and class_attr_name from its attributes.

    Args:
        doc (Document): Document to extract from
        attribute_names (array<str>): names to extract
        class_attr_name (str): Class attr to extract (deleted before classification)

    Returns:
        A dict of the retrieved values
    """
    feature_dict = {}
    feature_dict[class_attr_name] = doc.attrs[class_attr_name]

    for attr_key in attribute_names:
        attr_val = doc.attrs[attr_key]
        feature_dict[attr_key] = attr_val

    return feature_dict

def convert_documents_to_feature_dict(docs, attribute_names, class_attr_name):
    """
    Converts documents into dicts by extracting attribute_names and class_attr_name from their attributes.

    Args:
        docs (iter<Document>): Documents to extract from
        attribute_names (array<str>): names to extract
        class_attr_name (str): Class attr to extract (deleted before classification)

    Returns:
        A generator yielding dicts of the extracted values
    """
    for doc in docs:
        yield convert_document_to_feature_dict(doc, attribute_names, class_attr_name)

def apply_transforms(feature_transforms, feature_dicts, do_train_transforms):
    """
    Applies (and trains, where applicable) transforms to documents.

    Args:
        feature_transforms (array<Callable>) and array of feature transforms.
            A feature transform must expect a dict and return this or a new dict. Transforms are allowed to work in-place on the dicts passed to them.
        feature_dicts (iter<dict>) Feature dicts to process
        do_train_transforms (boolean) If true, train features (e.g. attribute weights) on the feature_dicts before appyling.

    Returns:
        Transformed feature dicts.
    """
    print "Starting transform"
    feature_dicts = list(feature_dicts)
    tranform_step = 0

    while True:
        curr_feature_transforms = {}

        for feature_name, transform in feature_transforms.items():
            if isinstance(transform, list) and len(transform) > tranform_step:
                curr_feature_transforms[feature_name] = transform[tranform_step]
            elif tranform_step == 0:
                curr_feature_transforms[feature_name] = transform

        if len(curr_feature_transforms) == 0:
            break
        else:
            print "Running transform step {}".format(tranform_step)

        if do_train_transforms:
            for feat_dict in feature_dicts:
                for feature_name, transform in curr_feature_transforms.items():
                    transform.train(feat_dict[feature_name])

        for feat_dict in feature_dicts:
            for feature_name, transform in curr_feature_transforms.items():
                feat_dict[feature_name] = transform.apply(feat_dict[feature_name])

        tranform_step += 1

    print "Transform complete"

    return feature_dicts

def flatten_feature_dict(feat_dict, class_attr_name):
    """
    Converts a hierarchical feature dict of the form {"feat_name": {"feat_value": value}}
    to a flat dict of the form {("feat_name", "feat_value"): value}. Also eliminates the class_attr from the feature dict.

    Args:
        feat_dict (dict): The hierarchical feature dict to convert
        class_attr_name (str): The name of the class attribute in the feature dict.

    Returns:
        A two tuple. First value is the flat feature dict, second the value of the class attribute of the dict.
    """
    flat_feature_dict = {}
    for key in feat_dict:
        if key != class_attr_name:
            flat_feature_dict.update(retrieve_feature_dict_values(feat_dict, key))

    return flat_feature_dict, feat_dict[class_attr_name]

def flatten_feature_dicts_and_extract_class(feature_dicts, class_attr_name):
    """
    Converts hierarchical feature dicts of the form {"feat_name": {"feat_value": value}}
    to flat dicts of the form {("feat_name", "feat_value"): value}. Also eliminates the class_attr from the feature dicts.

    Args:
        feature_dicts (iter<dict>): The hierarchical feature dicts to convert
        class_attr_name (str): The name of the class attribute in the feature dicts.

    Returns:
        A two tuple. First value is an array of flat feature dicts, second the class attribute values of the dicts
    """
    flat_feat_dicts = []
    class_vals = []
    for feat_dict in feature_dicts:
        flat_feature_dict, class_val = flatten_feature_dict(feat_dict, class_attr_name)
        flat_feat_dicts.append(flat_feature_dict)
        class_vals.append(class_val)

    return flat_feat_dicts, class_vals

def vectorize_feature_dicts(feature_dicts, class_attr_name, vectorizer = None, encoder = None, feature_transforms = {}, do_train_transforms = True):
    """
    Converts feature dicts into a scikit learn feature matrix and and a class vector

    Args:
        * feature_dicts (iter<dict>): Feature dicts to process
        * class_attr_name (str): The class attr in the feature dicts
        * vectorizer (scikit vectorizer): The vectorizer to use. If None, a new one will be created.
        * encoder (scikit label encoder): The label encoder vectorizer to use. If None, a new one will be created.
        * feature_transforms (dict<FeatureTransform>): The feature transforms to use on the features identified by the dict keys
        * do_train_transforms (boolean): If true, trains encoders

    Returns:
        A five tuple:
            1. (Matrix): A feature matrix derived from feature_dicts
            2. (Vector): A class vector derived from feature_dicts
            3. (scikit vectorizer): The vectorizer used
            4. (scikit label encoder): The label encoder used
            5. feature_transforms: The feature transforms used (trained, if do_train_transforms == True)
    """
    feature_dicts = apply_transforms(feature_transforms, feature_dicts, do_train_transforms)

    flat_feature_dicts, class_labels = flatten_feature_dicts_and_extract_class(feature_dicts, class_attr_name)

    if vectorizer:
        vectors = vectorizer.transform(flat_feature_dicts)
    else:
        vectorizer = DictVectorizer(sparse = True)
        vectors = vectorizer.fit_transform(flat_feature_dicts).toarray()

    if encoder:
        class_vector = encoder.transform(class_labels)
    else:
        encoder = LabelEncoder()
        class_vector = encoder.fit_transform(class_labels)

    return vectors, class_vector, vectorizer, encoder, feature_transforms

def vectorize_documents(
        docs,
        attribute_names,
        class_attr_name,
        vectorizer = None,
        encoder = None,
        feature_transforms = {},
        do_train_transforms = True):
    """
    Vectorizes documents by extracting attribute_names into feature dicts. See vectorize_feature_dicts for details on arguments and return values.
    """
    feature_dicts = convert_documents_to_feature_dict(docs, attribute_names, class_attr_name)
    return vectorize_feature_dicts(feature_dicts, class_attr_name, vectorizer, encoder, feature_transforms, do_train_transforms = do_train_transforms)

def cross_validate_attribute_set(docs, attribute_names, class_attr_name, feature_transform_config):
    """
    Cross validates classifier on docs five-fold. Results go to stdout.

    Args:
        docs (iter<Document>): Documents to evaluate on
        attribute_names (array<str>): Document attributes to use as features
        class_attr_name (str): Document attribute to use as class
        feature_transform_config (dict): Feature transform config to create transforms from
    """
    feature_transforms = load_feature_transforms_from_config(feature_transform_config, attribute_names)
    vectors, class_vector, vectorizer, labeler, feature_transform_config = vectorize_documents(docs, attribute_names, class_attr_name, feature_transforms = feature_transforms)
    classifier = svm.LinearSVC()
    print "Starting Cross-Validation"
    predictions = cross_validation.cross_val_predict(classifier, vectors, class_vector, cv = 5)

    print confusion_matrix(class_vector, predictions)
    print classification_report(class_vector, predictions)
    print labeler.inverse_transform(range(0, 11))

def train(docs, attribute_names, class_attr_name, model_folder, feature_transform_config):
    """
    Trains a model on docs.

    Args:
        docs (iter<Document>): Documents to train on
        attribute_names (array<str>): Document attributes to use as features
        class_attr_name (str): Document attribute to use as class
        model_folder (str): Path of folder to persist model to.
        feature_transform_config (dict): Feature transform config to create transforms from

    """
    filenames = get_model_filenames(model_folder)

    if os.path.exists(model_folder):
        answer = None
        while answer == None:
            print "Model exists, retrain?[Y/n]"
            user_answer = raw_input()
            if user_answer == "Y":
                answer = True
            elif user_answer == "n":
                answer = False
        if answer == True:
            shutil.rmtree(model_folder)
            os.mkdir(model_folder)
        else:
            return
    else:
        os.mkdir(model_folder)

    feature_transforms = load_feature_transforms_from_config(feature_transform_config, attribute_names)

    vectors, class_vector, vectorizer, encoder, feature_transform_config = vectorize_documents(docs, attribute_names, class_attr_name, feature_transforms = feature_transforms, do_train_transforms = True)

    classifier = svm.LinearSVC()
    classifier.fit(vectors, class_vector)

    joblib.dump(classifier, filenames["classifier"])
    joblib.dump(encoder, filenames["encoder"])
    joblib.dump(vectorizer, filenames["vectorizer"])
    joblib.dump(feature_transforms, filenames["transforms"])

def test(docs, attribute_names, class_attr_name, model_folder):
    """
    Evaluate a model on docs.

    Args:
        docs (iter<Document>): Documents to train on
        attribute_names (array<str>): Document attributes to use as features
        class_attr_name (str): Document attribute to use as class
        model_folder (str): Path of folder to load model from.
    """

    filenames = get_model_filenames(model_folder)

    classifier = joblib.load(filenames["classifier"])
    encoder = joblib.load(filenames["encoder"])
    vectorizer = joblib.load(filenames["vectorizer"])
    feature_transforms = joblib.load(filenames["transforms"])

    vectors, class_vector, _, _, _ = vectorize_documents(docs, attribute_names, class_attr_name, vectorizer = vectorizer, encoder = encoder, feature_transforms = feature_transforms, do_train_transforms = False)

    predictions = classifier.predict(vectors)

    print confusion_matrix(class_vector, predictions)
    print classification_report(class_vector, predictions)
    print encoder.inverse_transform(range(0, 11))

def get_model_filenames(base_folder):
    """
    Returns filenames for model files relative to base_folder

    Args:
        base_folder (str): Base folder for model

    Returns:
        a dict of file keys and corresponding paths.
        Current file keys are:
            * classifier: classifier file
            * encoder: encoder file
            * vectorizer: vectorizer file
            * transforms: file to store trained transforms
    """
    filenames = {
            "classifier": "classifier.pkl",
            "encoder": "encoder.pkl",
            "vectorizer": "vectorizer.pkl",
            "transforms": "transforms.pkl"
            }

    full_filenames = {}

    for key, filename in filenames.items():
        full_filenames[key] = base_folder + "/" + filenames[key]

    return full_filenames

