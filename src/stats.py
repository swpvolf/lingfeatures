import math
from collections import Counter

def calculate_pmis(docs, attr1_name, attr2_name, event1_converter = lambda v: v, event2_converter = lambda v: v):
    attr1_distr = Counter()
    attr2_distr = Counter()
    coocurrence_distr = Counter()
    n_docs = 0
    for doc in docs:
        attr1_val = event1_converter(doc.attrs[attr1_name])
        attr2_val = event2_converter(doc.attrs[attr2_name])

        attr1_distr[attr1_val] += 1
        attr2_distr[attr2_val] += 1

        coocurrence_distr[(attr1_val, attr2_val)] += 1

        n_docs += 1

    result = {}
    for val1 in attr1_distr:
        for val2 in attr2_distr:
            result[(val1, val2)] = calculate_pmi_from_counts(
                    attr1_distr[val1],
                    attr2_distr[val2],
                    coocurrence_distr[(val1, val2)],
                    n_docs
                    )
    return result

def calculate_npmi_from_counts(x_cnt, y_cnt, cooc_cnt, n_inst):
    p_cooc = float(cooc_cnt) / n_inst
    normalization_factor = -math.log(math.ceil(p_cooc))
    return calculate_pmi_from_counts(x_cnt, y_cnt, cooc_cnt, n_inst) / normalization_factor

def calculate_pmi_from_counts(x_cnt, y_cnt, cooc_cnt, n_inst):
    p_x = float(x_cnt) / n_inst
    p_y = float(y_cnt) / n_inst
    p_cooc = float(cooc_cnt) / n_inst
    return math.log(p_cooc / (p_x * p_y))
