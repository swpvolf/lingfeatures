from itertools import product
import sys
import traceback
import time
import json
import ast
import cPickle as pickle


"""
Short introduction to the pipeline module.

The pipeline module allows the user to apply a series of components on a document.
It provides resolution of run-order for dependencies of components and serialization of document annotations using pickle files.

Components may modify a document by providing annotations on attributes at different annotation levels.
Each attribute is identified by a string that is unique among the attributes at the annotation level it resides, the so called key.
Attributes are intended to ensure proper run-order of all components and as a documentation device.
The attributes of an item may be accessed using the attrs property of an item, e.g. item.attrs.key.

Components must declare which attributes they use using the following attributes,:
    - pypline__produced_attributes (list of Attr): The attributes annotated by the component
    - pypline__required_attributes (list of Attr): The attributes required to be annotated before the component can run
    - pypline__created_levels (list od Class): The new annotation levels introduced by the component

Convinience decorators are provided to allow for easy declaration of components.
"""

class Serializer:
    """
    Serializes and deserializes documents into and from pickle files inside a directory.
    """
    def __init__(self, base_dir):
        self.base_dir = base_dir

    def get_filename_for_doc_name(self, doc_name):
        return self.base_dir + "/" + doc_name + ".dump"

    def write_doc(self, doc):
        with open(self.get_filename_for_doc_name(doc.name), "w") as f:
            pickle.dump(doc, f)

    def read_doc(self, doc_name):
        try:
            with open(self.get_filename_for_doc_name(doc_name)) as f:
                return pickle.load(f)
        except IOError:
            return None

class Graph:
    """
    Simple graph class
    Stores nodes as an adjancency list.
    Nodes must be hashable
    """
    def __init__(self):
        self.adjacency_list = dict()

    def add_nodes(self, nodes):
        for node in nodes:
            self.add_node(node)

    def add_node(self, node):
        self.adjacency_list[node] = []

    def add_edge(self, orig, dest):
        self.adjacency_list[orig].append(dest)

    def dfs_with_cycle_detection(self, start):
        """
        Performs depth first search on the set starting from start. Tuples a yielded in preorder.

        Args:
            start (node): The node to start the dfs
        Returns:
            A generator which yields two tuples. First element is the node, second is True, if a cycle has been detected, False otherwise.
        """
        processing_list = [(start, [])]

        visited = set()

        while len(processing_list) > 0:
            node, visited_before = processing_list.pop()
            if node in visited_before:
                yield node, True
                continue
            if node in visited:
                continue
            visited.add(node)
            yield node, False

            for neighbour in self.adjacency_list[node]:
                processing_list.append((neighbour, visited_before + [node]))

    def compute_topological_order(self):
        """
        Computes the topological order of the graph.

        Returns:
            A list of all nodes in topological order
        Raises:
            RuntimeError if the graph is not acyclic
        """
        result = []
        incoming_edges = dict([(n, [m for m, connected in self.adjacency_list.items() if n in connected]) for n in self.adjacency_list])
        start_nodes = [n for n in self.adjacency_list.keys() if len(incoming_edges[n]) == 0]

        while len(start_nodes) > 0:
            n = start_nodes.pop()
            result.append(n)
            for m in self.adjacency_list[n]:
                incoming_edges[m].remove(n)
                if len(incoming_edges[m]) == 0:
                    start_nodes.append(m)

        if len(result) < len(self.adjacency_list.keys()):
            raise RuntimeError("Could not compute topological ordering for {!r}. Graph is not acyclic.")

        return result

class Pipeline:
    """
    Backbone of data processing
    Reads documents from a corpus reader and processes them with the components passed to it.
    A component is a callable which accepts a Document as its only parameter
    A valid component must have both a __pypline_produced_attributes and a __pypline_required_attributes property,
    which specifies which data they need to be annotated in the documents passed to them.
    """
    def __init__(self, reader, components, ignore_exceptions = False, serializer = None):
        """
        Constructor
        
        Args:
            reader (iterator): an iterator which yields document promises.
                A document promise is a callable with a name property which must be an unique identifier for the document.
                Calling the callable should yield an instance of document.

            components (array): an array of components to run. Components must obey informal component interface.

            ignore_exceptions (boolean): If true, pipeline will catch and report exceptions instead of aborting.

            serializer: The document annotation persistance provider. See Serializer for details.

        """
        self.reader = reader
        self.components = components
        self.ignore_exceptions = ignore_exceptions

        #TODO: Fix
        self.serializer = serializer

        for comp in components:
            make_pipeline_component(comp)

    def _build_run_graph(self):
        """
        Builds a directed graph of all components to determine input/output dependencies.
        """
        graph = Graph()
        graph.add_node(self.reader)
        graph.add_nodes(self.components)
        for comp_1, comp_2 in product(self.components + [self.reader], self.components + [self.reader]):
            if comp_2 == self.reader:
                continue
            if comp_1 == comp_2:
                continue
            for attr in comp_1.pypline__produced_attributes:
                if attr in comp_2.pypline__required_attributes:
                    graph.add_edge(comp_1, comp_2)
                    break
        return graph

    def _determine_run_order(self):
        """
        Determines order in which to run components.

        Returns:
            An array of components in a correct run order.
        """
        graph = self._build_run_graph()
        run_order = []
        errors = []
        run_order = graph.compute_topological_order()
        run_order.remove(self.reader)

        return run_order, errors

    def _check_run_order(self, run_order):
        """
        Checks whether all depencies of the components are statisfied by a given run order.

        Args:
            run_order (array): A run order to check

        Returns:
            An array of errors where depencies are violated by the passed run order.
        """
        available_attrs = set(self.reader.pypline__produced_attributes)
        available_annotation_levels = {Document, Token}
        available_annotation_levels.update(self.reader.pypline__created_levels)

        errors = []

        for comp in run_order:
            for level in comp.pypline__created_levels:
                if level in available_annotation_levels:
                    errors.append(
    "Can not create level {}: already exists at component {!r}".format(level.__name__, comp))
                else:
                    available_annotation_levels.add(level)

            for attr in comp.pypline__required_attributes:
                if attr not in available_attrs:
                    errors.append(
    "Requirement {} not statisfied for component {!r}".format(attr.key, comp))
                if attr.level not in available_annotation_levels:
                    errors.append(
    "Level {} has not been introduced for component {!r}".format(attr.level.__name__, comp))
            available_attrs.update(comp.pypline__produced_attributes)

        self.available_attrs = available_attrs
        self.available_annotation_levels = available_annotation_levels

        return errors

    def _determine_components_to_run_on_document(self, document, run_order):
        """
        Determines which annotations are missing on a document and runs them accordingly.
        Currently does not support running components for dependant components as well, as we
        did not need the feature yet.
        """
        #TODO: Check required attributes in case conflicts arise
        comps_to_run = []
        for comp in run_order:
            for attr in comp.pypline__produced_attributes:
                if not document.has_attr_at_level(attr.level, attr.key):
                    comps_to_run.append(comp)
                    break

        return comps_to_run

    def run(self):
        """
        Runs the pipeline given the current configuration.
        Will determine run-order on startup.

        Returns:
            A generator which yields Document objects annoted by the components
        """
        start_time = time.clock()

        run_order, errors = self._determine_run_order()
        if len(errors) > 0:
            raise RuntimeError("\n".join(errors))
        errors = self._check_run_order(run_order)
        if len(errors) > 0:
            raise RuntimeError("\n".join(errors))

        for document_promise in self.reader:
            print "Processing document {} ...".format(document_promise.name)

            document = self.serializer.read_doc(document_promise.name)

            if document:
                print "Read document dump of {} ...".format(document.name)
                components_to_run = self._determine_components_to_run_on_document(document, run_order)
                if len(components_to_run) == 0:
                    did_modfiy_doc = False
                    print "Component up to date, no need to rerun"
                else:
                    did_modfiy_doc = True
                for comp in components_to_run:
                    print "Running {} on document {}".format(comp.__class__.__name__, document.name)
                    comp(document)

            else:
                did_modfiy_doc = True
                document = document_promise()
                try:
                    for component in run_order:
                        component(document)
                except Exception as e:
                    error_text = "Unhandled exception in document {}:\n{}".format(document.name, traceback.format_exc())
                    if self.ignore_exceptions:
                        print "WARNING:", error_text
                    else:
                        raise RuntimeError(error_text)

            if did_modfiy_doc:
                self.serializer.write_doc(document)
            yield document

        end_time = time.clock()
        run_time = end_time - start_time

        print "Pipeline ran {}".format(human_readable_timespan(run_time))

def human_readable_timespan(seconds):
    """
    Returns a human readable timespan (hours/minutes/seconds)

    Args:
        seconds (float): seconds to convert

    Returns:
        A string in the format <h*> h <mm> min <ss> s. If seconds is shorter than a minute, hour and minute is dropped.
        If seconds is shorter than an hour, hour is dropped.
    """
    minutes = int(seconds) / 60 #Purposeful integer division
    hours = minutes / 60

    remaining_minutes = minutes - (hours * 60)
    remaining_seconds = seconds - (minutes * 60)

    result_parts = []
    if hours > 0:
        result_parts.append("{} h".format(hours))
    if minutes > 0:
        result_parts.append("{} min".format(remaining_minutes))


    result_parts.append("{} s".format(remaining_seconds))

    return " ".join(result_parts)

class Attr:
    """
    Describes an attribute with a unique key on any annotation level.
    """
    def __init__(self, key, level):
        self.key = key
        self.level = level

    def __eq__(self, other):
        return self.key == other.key and self.level == other.level

    def __hash__(self):
        return hash((self.key, self.level))

    def __repr__(self):
        return "Attr({}, {})".format(self.key, self.level)

class StandardAttrDescriptor(object):
    """
    Standard Attr descriptor used in annotation storage.
    """
    def __init__(self, val):
        self.val = val

    def __get__(self, obj, objtype):
        return self.val

def produces(key, level):
    """
    Convenience decorator to populate pypline__produced_attributes.
    """
    def changer(cls):
        make_pipeline_component(cls)
        cls.pypline__produced_attributes.append(Attr(key, level))
        return cls
    return changer

def requires(key, level):
    """
    Convenience decorator to populate pypline__required_attributes.
    """
    def changer(cls):
        make_pipeline_component(cls)
        cls.pypline__required_attributes.append(Attr(key, level))
        return cls
    return changer

def requires_span_level(level):
    """
    Not currently implemented, because it wasn't needed for the application.
    """
    def changer(cls):
        make_pipeline_component(cls)
        return cls
    return changer

def creates_span_level(level):
    """
    Convenience decorator to populate pypline__created_levels.
    """
    def changer(cls):
        make_pipeline_component(cls)
        cls.pypline__created_levels.append(level)
        return cls
    return changer

def make_pipeline_component(cls):
    """
    Makes a class a valid component.

    Args:
        cls (Class): A class to make a component of.
    """
    CMP_ATTRS = [
            "pypline__produced_attributes",
            "pypline__required_attributes",
            "pypline__created_levels"]
    for attr in CMP_ATTRS:
        if not hasattr(cls, attr):
            setattr(cls, attr, [])

class AnnotationLevel(object):
    """
    Abstract Annotation Level Base Class
    """
    def __init__(self):
        self.attrs = AttrWrapper()

    def __repr__(self):
        cls_name = type(self).__name__

        attr_list = map(lambda k: "{}={!r}".format(k[0], k[1]), self.attrs._values.items())

        return "{}({})".format(cls_name, ", ".join(attr_list))

    def has_attr(self, key):
        return self.attrs._has_attr(key)

class Document(AnnotationLevel):
    """
    Highest level Annotation Level.

    A document consists of an array of Tokens.
    There may also be SpanAttributes, which include multiple tokens.
    """
    def __init__(self):
        super(Document, self).__init__()
        self.spans = {}
        self.tokens = []

    def add_span(self, span):
        self.spans.setdefault(type(span), []).append(span)

    def add_token(self, token):
        self.tokens.append(token)

    @property
    def all_tokens(self):
        """
        Returns all tokens of the document.

        Returns:
            An array of Token objects.
        """
        return self.tokens

    def get_all_annotation_items_at_level(self, level):
        """
        Gets all items of the specified annotation level.

        Args:
            level (Class): The annotation class to consider

        Returns:
            An array of "level" objects.
        """

        if level == Token:
            return self.all_tokens
        else:
            return sorted(self.spans.get(level, []), key=lambda s: s.start_idx)

    def has_attr_at_level(self, level, key):
        """
        Checks whether the attribute key exists at annotation level "level". WARNING: Assumes annotations are always consistent accross multiple instances of the level.

        Args:
            level (Class): The annotation class to consider
            key (str): The name of the attribute to check.

        Returns:
            An array of "level" objects.
        """

        if level == Document:
            return self.has_attr(key)
        elif level == Token:
            if len(self.all_tokens) == 0:
                return False
            else:
                return self.all_tokens[0].has_attr(key)
        else:
            span_items = self.spans.get(level, [])
            if len(span_items) == 0:
                return False
            else:
                return span_items[0].has_attr(key)

class AttrWrapper(object):
    """
    Dictionary like object that allows property access and square bracket access
    """
    def __init__(self):
        self._values = {}

    def __setattr__(self, key, val):
        if key.startswith("_"):
            super(AttrWrapper, self).__setattr__(key, val)
        else:
            self._values[key] = val

    def __getattr__(self, key):
        if key.startswith("_"):
            res = super(AttrWrapper, self).__getattr__(key)
            return res
        else:
            return self._values[key]

    def __getitem__(self, key):
        return self._values[key]

    def __setitem__(self, key, val):
        self._values[key] = val

    def _as_dict(self):
        return self._values

    def _has_attr(self, key):
        return key in self._values

class TokenSpan(AnnotationLevel):
    """
    A base class for all span properties.
    """
    def __init__(self):
        super(TokenSpan, self).__init__()
        self.tokens = []

    def add_token(self, token):
        self.tokens.append(token)

    @property
    def start_idx(self):
        return min(map(lambda t: t.idx, self.tokens))

class Sentence(TokenSpan):
    """
    A token span annotation level representing a sentence.
    """

    def __init__(self):
        super(Sentence, self).__init__()

class Token(AnnotationLevel):
    """
    A token span annotation level representing a single token.
    """
    def __init__(self):
        super(Token, self).__init__()

