This project contains our work on our system for native language identificartion build during the software project in the winter term 2015/16.

Authors
-------
Bettina Emmerich (emmerich@cl.…)
Julius Steen (steen@cl.…)

Versions
-------
1.0: This version, handed in for the software project


Dependencies
------------

* python 2.7
* The PDTB parser of Lin et. al. (https://github.com/linziheng/pdtb-parser), requires a ruby installation.
* NLTK
* Stanford POS-Tagger (NLTK bindings)
* scikit-learn
* pyaml


Environment Variables
--------------------

* PDTB_PARSER_PATH: The path to the root directory of the pdtb parser.


Usage
-----

Our system has a comprehensive configuration system based on YAML files.
To run a train/test cycle on the whole corpus type:
    python nlipipeline.py full.cfg
in the src directory.


Configuration files
-------------------

Valid entries:
    * reader (dict): Reader configuration
        * directory (str): path to TOFEL11 file directory
        * proficiencies (array<str>): proficiencies to consider, valid values: high, low, medium
        * limit: maximum number of files to read
    * features (array<str>): Array of document level attribute keys to consider for classification.
    * transforms (dict<array<str|dict<dict>>>): Transforms to apply to the features in "features".
        The feature identfied by "key" will be transformed sequentially by the transformers identified by the items in the array passed to it.
        If passed a str, the string must identify a transformer.
        If passed a dict, the dict must have a single top level entry with the name of the transformer that contains an array configuration options.
        Feature keys may be regexes and will affect all matching features.

    * model <str>: The directory to write the model to. Should be an empty or non-existing directory.

    * class_attr <str>: The document level attribute to use as the class in the classification problem.

    * test_method <str>: The test method to employ. Valid values are traintest, for evaluation on the trainingset and crossvalidation for 5-fold cross validation.

Available Transforms:
    * global_frequency_cutoff: Removes infrequent features
        parameters:
            * threshold: Number of times a features must occur in order not to be dropped
    * log_entropy_weighting: Weights features using log-entropy weighting
    * relative_weighting: Weights all features relative to total occurence in the same class.


Files
-----

* nlipipeline.py: Main project file, contains all NLI specific feature extraction routines and bindings for the external POS-Tagger and PDTB-Parser
* pipeline.py: A generic pipeline framework used by nlipipeline to structure data processing
* classifier.py: Contains classification and feature weighting routines

